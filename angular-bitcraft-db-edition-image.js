(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

var template = '' +
'<img ng-if="!$ctrl.edit" ng-src="{{$ctrl.value}}">' +
'<input class="form-control" ng-if="$ctrl.edit" type="text" ng-model="$ctrl.value">' +
'';


//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition-image')
    .component('bitcraftDbEditionImage', {
        template: template,
        bindings: {
            value: '=',
            edit: '<'
        }
    });

},{}],2:[function(require,module,exports){
/**
 * Created by richard on 19/10/16.
 */

angular.module('bitcraft-db-edition-image', []);

},{}],3:[function(require,module,exports){
/**
 * Created by richard on 19/10/16.
 */

'use strict';
require('./db-edition-image.module.js');
require('./db-edition-image.component.js');

},{"./db-edition-image.component.js":1,"./db-edition-image.module.js":2}]},{},[3]);
