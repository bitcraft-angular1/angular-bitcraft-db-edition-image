/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition-image')
    .component('bitcraftDbEditionImage', {
        templateUrl: './js/db-edition-image/db-edition-image.template.html', // this line will be replaced
        bindings: {
            value: '=',
            edit: '<'
        }
    });
